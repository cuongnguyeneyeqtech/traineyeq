﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainEyeQTech.Model
{
    internal class QueryReponse
    {
        
        public class Data
        {
            public Data()
            {
                this.actionType = "";
                this.add_border = null;
                this.aliasId = "";
                this.biggest_face = "";
                this.check_spoof = false;
                this.client_id = 0;
                this.confident = 0;
                this.distance = 1;
                this.hasGlasses = false;
                this.hasMask = false;
                this.initTimestamp = 0;
                this.invalid_faces = false;
                this.is_spoof = false;
                this.organization = "";
                this.possible_ids = null;
                this.receiveTimestamp = 0;
                this.strict = "";
                this.thres = "";
            }

            public string actionType { get; set; }
            public object add_border { get; set; }
            public string aliasId { get; set; }
            public string biggest_face { get; set; }
            public bool check_spoof { get; set; }
            public double client_id { get; set; }
            public double confident { get; set; }
            public double distance { get; set; }
            public bool hasGlasses { get; set; }
            public bool hasMask { get; set; }
            public double initTimestamp { get; set; }
            public bool invalid_faces { get; set; }
            public bool is_spoof { get; set; }
            public string organization { get; set; }
            public List<PossibleId> possible_ids { get; set; }
            public double receiveTimestamp { get; set; }
            public string strict { get; set; }
            public string thres { get; set; }
        }

        public class PossibleId
        {
            public PossibleId()
            {
                this.aliasId = "";
                this.confident = 0;
                this.distance = 1;
            }

            public string aliasId { get; set; }
            public double confident { get; set; }
            public double distance { get; set; }
        }

        public QueryReponse()
        {
            data = new Data();
            message = "";
            status = "";
        }
        public Data data { get; set; }
        public string message { get; set; }
        public string status { get; set; }
    }
}
