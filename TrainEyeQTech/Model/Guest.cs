﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Toolkit.Mvvm.ComponentModel;

namespace TrainEyeQTech.Model
{
    internal class Guest : ObservableObject
    {

        private string bookingID;
        private string name;

        public Guest()
        {
            this.name = "";
            this.bookingID = "";
        }
        public Guest(string bookingID, string name)
        {
            this.name = name;
            this.bookingID = bookingID;
        }
        public string BookingID
        {
            get => bookingID;
            set => SetProperty(ref bookingID, value);
        }
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
    }
}
