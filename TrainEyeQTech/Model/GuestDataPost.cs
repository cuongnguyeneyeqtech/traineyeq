﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Toolkit.Mvvm.ComponentModel;

namespace TrainEyeQTech.Model
{
    internal class GuestDataPost : ObservableValidator
    {
        private string aliasID;
        private string images;

        public string AliasID
        {
            get => aliasID;
            set => SetProperty(ref aliasID, value);
        }
        public string Images
        {
            get => images;
            set => SetProperty(ref images, value);
        }


        public GuestDataPost(string aliasID, string images)
        {
            this.aliasID = aliasID;
            this.images = images;
        }
    }
}
