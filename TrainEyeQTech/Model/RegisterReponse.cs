﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainEyeQTech.Model
{
    internal class RegisterReponse : ObservableValidator
    {
        public RegisterReponse()
        {
            code = "0000";
            message = "";
            status = "";
        }
        public string code { get; set; }
        public string message { get; set; }
        public string status { get; set; }
    }
}
