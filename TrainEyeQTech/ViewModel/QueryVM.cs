﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using TrainEyeQTech.Model;

namespace TrainEyeQTech.ViewModel
{
    internal class QueryVM : ObservableRecipient
    {
        private Image image;
        public QueryVM(Image image)
        {
            this.image = image;
            QueryCommand = new RelayCommand(QueryGuest);
        }

        private string booking_id;
        private string name;
        private string aliasId;

        public string BookingId
        {
            get {
                try
                {
                    return aliasId.Split('_')[0];
                }
                catch { return aliasId; }
            }
            set => SetProperty(ref booking_id, aliasId.Split('_')[0]);
        }
        public string Name
        {
            get {
                try
                {
                    return aliasId.Split('_')[1];
                }catch { return ""; }
            }
            set => SetProperty(ref name, aliasId.Split('_')[1]);
        }
        public string AliasId
        {
            get => aliasId;
            set => SetProperty(ref aliasId, value);
        }

        private string message;
        private bool successed;
        private string status;
        public string Message
        {
            get => message;
            set => SetProperty(ref message, value);
        }

        public string Status
        {
            get => status; set => SetProperty(ref status, value);
        }

        public bool Successed
        {
            get => status == "successful" ? true : false;
            set => SetProperty(ref successed, status == "successful" ? true : false);
        }

        public ICommand QueryCommand { get; }
        public ICommand JsonCommand { get;  }

        private DataTable infGuest;

        public DataTable InfGuest
        {
            get => infGuest;
            set => SetProperty(ref infGuest, value);
        }

        private void QueryGuest()
        {
            string queryPath = Application.StartupPath + "\\query.png";
            
            image.Save(queryPath);
            var jsonAsync = API.Recognize(queryPath);
            jsonAsync.Wait();
            string jsonString = jsonAsync.Result;

            Console.WriteLine(jsonString);

            //string jsonString = File.ReadAllLines(Application.StartupPath + "\\_viewModelQuery.txt")[0];

            QueryReponse queryReponse = JsonConvert.DeserializeObject<QueryReponse>(jsonString);
            if (queryReponse.data.aliasId != "")
            {
                this.AliasId = queryReponse.data.aliasId;
                this.Status = queryReponse.status;
                List<Guest> guests = new List<Guest>();
                guests.Add(new Guest(this.BookingId, this.Name));
                foreach(object guest in queryReponse.data.possible_ids)
                {
                    string id;
                    string name;
                    try
                    {
                        id = ((QueryReponse.PossibleId)guest).aliasId.Split('_')[0];
                        name = ((QueryReponse.PossibleId)guest).aliasId.Split('_')[1];
                    }
                    catch
                    {
                        id = ((QueryReponse.PossibleId)guest).aliasId;
                        name = "";
                    }
                    guests.Add(new Guest(id, name));
                }
                this.InfGuest = CreateDataTable<Guest>(guests);
            }

            this.Message = queryReponse.message;
            
        }

        private DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            dataTable.TableName = typeof(T).FullName;
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

    }
}
