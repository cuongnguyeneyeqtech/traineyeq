﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Newtonsoft.Json;
using TrainEyeQTech.Model;

namespace TrainEyeQTech.ViewModel
{
    internal class RegisterVM : ObservableValidator
    {
        
        private GuestDataPost guestData;
        public GuestDataPost GuestData
        {
            get => guestData;
        }
        public RegisterVM(string aliasId, Image img)
        {
            string urlImage = Application.StartupPath + "\\register.png";
            img.Save(urlImage);
            this.guestData = new Model.GuestDataPost(aliasId, urlImage);
            RegisterCommand = new RelayCommand(RegisterUser);
        }

        //private string jsonString;
        private string status;
        private string urldata;
        private bool succeeded;
        private string message;
        public bool Succeeded
        {
            get { return status=="successful" ? true:false; }
            set => SetProperty(ref succeeded, status == "successful" ? true : false);
        }
        public string Status
        {
            get => status;
            set => SetProperty(ref status, value);
        }
        public string UrlData
        {
            get => urldata;
            set => SetProperty(ref urldata, value);
        }
        public string Meesage
        {
            get => message;
            set => SetProperty(ref message, value);
        }

        public ICommand RegisterCommand { get; }

        private void RegisterUser()
        {
            //guestData use here with API
            var register = API.Register(guestData);
            register.Wait();
            string jsonString = register.Result;

            RegisterReponse guestReponse = JsonConvert.DeserializeObject<RegisterReponse>(jsonString);
            this.Status = guestReponse.status;
            if (this.Succeeded)
                this.Meesage = "Registered successfully";
            else
            {
                if (guestReponse.code == "0002" || guestReponse.code == "0005")
                    this.Meesage = guestReponse.message;
                else
                    this.Meesage = guestData.AliasID.Split('_')[1] + " already registered";
            }
            Console.WriteLine(jsonString);

        }
    }
}
