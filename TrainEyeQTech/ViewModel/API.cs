﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainEyeQTech.Model;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Newtonsoft.Json;
using System.IO;
using System.Drawing.Imaging;

namespace TrainEyeQTech.ViewModel
{

    internal static class API
    {
        private static string[] token = File.ReadAllLines(Application.StartupPath + "\\token.txt");
        
        public static async Task<string> Recognize(string filePath)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token[0]);

            using (var multipartFormContent = new MultipartFormDataContent())
            {
                var fileStreamContent = new StreamContent(File.OpenRead(filePath));
                fileStreamContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                multipartFormContent.Add(fileStreamContent, name: "images[]", fileName: "image.png");

                var response = await httpClient.PostAsync("https://endpointh.eyeq.tech/api/faceid/recognize", multipartFormContent);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
        }

        public static async Task<string> Register(GuestDataPost guest)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token[1]);

            //"D:\\CLOUDX\\EyeQTech\\faceid\\itsme.jpg"
            string filePath = guest.Images;
            string name = Path.GetFileName(filePath);

            using (var multipartFormContent = new MultipartFormDataContent())
            { 
                var fileStreamContent = new StreamContent(File.OpenRead(filePath));
                fileStreamContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                multipartFormContent.Add(fileStreamContent, name: "images[]", fileName: name);

                multipartFormContent.Add(new StringContent(guest.AliasID), name: "aliasId");

                var response = await httpClient.PostAsync("https://endpointh.eyeq.tech/api/faceid/register", multipartFormContent);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                Console.WriteLine(content);
                return content;
            }
        }
    }
}