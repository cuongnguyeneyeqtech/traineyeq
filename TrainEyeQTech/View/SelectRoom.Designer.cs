﻿namespace TrainEyeQTech.View
{
    partial class SelectRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.room1 = new System.Windows.Forms.Button();
            this.room2 = new System.Windows.Forms.Button();
            this.room3 = new System.Windows.Forms.Button();
            this.lbTitle = new System.Windows.Forms.Label();
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.pnlRoom = new System.Windows.Forms.Panel();
            this.pnlTitle.SuspendLayout();
            this.pnlRoom.SuspendLayout();
            this.SuspendLayout();
            // 
            // room1
            // 
            this.room1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.room1.BackColor = System.Drawing.SystemColors.Control;
            this.room1.Location = new System.Drawing.Point(100, 40);
            this.room1.Name = "room1";
            this.room1.Size = new System.Drawing.Size(120, 60);
            this.room1.TabIndex = 0;
            this.room1.Text = "Room 1";
            this.room1.UseVisualStyleBackColor = false;
            this.room1.Click += new System.EventHandler(this.room_Click);
            // 
            // room2
            // 
            this.room2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.room2.BackColor = System.Drawing.SystemColors.Control;
            this.room2.Location = new System.Drawing.Point(290, 40);
            this.room2.Name = "room2";
            this.room2.Size = new System.Drawing.Size(120, 60);
            this.room2.TabIndex = 0;
            this.room2.Text = "Room 2";
            this.room2.UseVisualStyleBackColor = false;
            this.room2.Click += new System.EventHandler(this.room_Click);
            // 
            // room3
            // 
            this.room3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.room3.BackColor = System.Drawing.SystemColors.Control;
            this.room3.Location = new System.Drawing.Point(480, 40);
            this.room3.Name = "room3";
            this.room3.Size = new System.Drawing.Size(120, 60);
            this.room3.TabIndex = 0;
            this.room3.Text = "Room 3";
            this.room3.UseVisualStyleBackColor = false;
            this.room3.Click += new System.EventHandler(this.room_Click);
            // 
            // lbTitle
            // 
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbTitle.Location = new System.Drawing.Point(0, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(681, 148);
            this.lbTitle.TabIndex = 19;
            this.lbTitle.Text = "Title";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbTitle.UseWaitCursor = true;
            // 
            // pnlTitle
            // 
            this.pnlTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTitle.Controls.Add(this.lbTitle);
            this.pnlTitle.Location = new System.Drawing.Point(1, 141);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Size = new System.Drawing.Size(681, 148);
            this.pnlTitle.TabIndex = 20;
            // 
            // pnlRoom
            // 
            this.pnlRoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRoom.Controls.Add(this.room2);
            this.pnlRoom.Controls.Add(this.room3);
            this.pnlRoom.Controls.Add(this.room1);
            this.pnlRoom.Location = new System.Drawing.Point(1, 293);
            this.pnlRoom.Name = "pnlRoom";
            this.pnlRoom.Size = new System.Drawing.Size(681, 158);
            this.pnlRoom.TabIndex = 21;
            // 
            // SelectRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.pnlTitle);
            this.Controls.Add(this.pnlRoom);
            this.Name = "SelectRoom";
            this.Text = "SelectRoom";
            this.Load += new System.EventHandler(this.SelectRoom_Load);
            this.pnlTitle.ResumeLayout(false);
            this.pnlRoom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.Label lbTitle;
        protected System.Windows.Forms.Panel pnlTitle;
        public System.Windows.Forms.Button room1;
        public System.Windows.Forms.Button room2;
        public System.Windows.Forms.Button room3;
        public System.Windows.Forms.Panel pnlRoom;
    }
}