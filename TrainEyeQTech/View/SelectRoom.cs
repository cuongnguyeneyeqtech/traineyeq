﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainEyeQTech.View
{
    public partial class SelectRoom : Form
    {
        public SelectRoom()
        {
            InitializeComponent();
        }

        public virtual void SelectRoom_Load(object sender, EventArgs e)
        {}

        #region choose room
        public static event EventHandler btnRoomClick;
        public void room_Click(object sender, EventArgs e)
        {
            if (btnRoomClick != null) 
                btnRoomClick(sender, e);
        }
        #endregion
    }
}
