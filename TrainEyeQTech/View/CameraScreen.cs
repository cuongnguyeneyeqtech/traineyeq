﻿using AForge.Video;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainEyeQTech.View
{
    public partial class CameraScreen : Form
    {

        public DataTable InfGuest { get; set; }
        public CameraScreen()
        {
            InitializeComponent();
        }

        public virtual void Cap(object sender, NewFrameEventArgs eventArgs) {}

    }
}
