﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Receptionist
{
    public partial class SelectRoomRecp : TrainEyeQTech.View.SelectRoom
    {
        public SelectRoomRecp()
        {
            InitializeComponent();
        }

        public override void SelectRoom_Load(object sender, EventArgs e)
        {
            lbTitle.Text = "Wait for guest select their room or help them";
        }

        public static event EventHandler btnNextSign;
        public static event EventHandler btnPrevInfGuest;

        private bool isChooseRoom()
        {
            foreach (Control ctrl in pnlRoom.Controls)
            {
                if (ctrl.GetType() == typeof(Button))
                {
                    if (ctrl.BackColor == SystemColors.ActiveCaption)
                        return true;
                }
            }
            return false;
        }
        public void btnNext_Click(object sender, EventArgs e)
        {
            if (btnNextSign != null && isChooseRoom())
                btnNextSign(sender, e);
            else
            {
                MessageBox.Show("Please choose the room!");
            }
        }

        public void btnBack_Click(object sender, EventArgs e)
        {
            if (btnPrevInfGuest != null)
                btnPrevInfGuest(sender, e);
        }
    }
}
