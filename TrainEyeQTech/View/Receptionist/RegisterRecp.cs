﻿using AForge.Video;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Toolkit.Mvvm;
using TrainEyeQTech.ViewModel;
using System.Threading;

namespace TrainEyeQTech.View.Receptionist
{
    public partial class RegisterRecp : TrainEyeQTech.View.CameraScreen
    {
        private CameraScreen guestPage;
        public RegisterRecp(CameraScreen guestPage)
        {
            InitializeComponent();
            this.guestPage = guestPage;
        }

        public override void Cap(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap src = (Bitmap)eventArgs.Frame.Clone();



            int h = pctCapture.Height;
            Double ratio = Convert.ToDouble(h) / src.Height;
            int w = Convert.ToInt32(ratio * src.Width);

            Bitmap target = new Bitmap(src, new Size(w, h));

            Rectangle cropRect = new Rectangle((target.Width - pctCapture.Width) / 2 - 20, 0, pctCapture.Width, pctCapture.Height);

            src = new Bitmap(cropRect.Width, cropRect.Height);

            using (Graphics g = Graphics.FromImage(src))
            {
                g.DrawImage(target, new Rectangle(0, 0, src.Width, src.Height),
                                 cropRect,
                                 GraphicsUnit.Pixel);
            }

            src.RotateFlip(RotateFlipType.Rotate180FlipY);

            guestPage.pctCapture.Image = src;
            pctCapture.Image = src;
        }


        private void btnTakePic_Click(object sender, EventArgs e)
        {
            //first is stop camera
            Screens.ReceptionistScreen.videoCap.SignalToStop();
            //API.Delete();


        }

        private void runVM(object obj)
        {
            RegisterVM register = (RegisterVM)obj;
            register.RegisterCommand.Execute(register);
        }
        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (pctCapture.Image != null)
            {
                string bookingID = txtBookingID.Text.Trim();
                string name = txtName.Text.Trim();
                string aliasId = bookingID + "_" + name;

                if (View.Screens.ReceptionistScreen.videoCap.IsRunning)
                {
                    MessageBox.Show("Please take picture of guest!");
                }
                else if (aliasId.Length <= 1 || bookingID.Length == 0 || name.Length == 0)
                {
                    MessageBox.Show("Please field name and booking of the guest!");
                }
                else if (aliasId.Length > 20)
                {
                    MessageBox.Show("Name and bookingID so long!");
                }
                else
                {
                    //viewModel
                    RegisterVM register = new RegisterVM(aliasId, pctCapture.Image);
                    Thread vm = new Thread(new ParameterizedThreadStart(runVM));
                    vm.Start(register);
                    Thread.Sleep(5000);

                    MessageBox.Show(register.Meesage);
                    if (!register.Succeeded)
                        Screens.ReceptionistScreen.videoCap.Start();
                    else
                        if (btnSkipRegister != null)
                    {
                        btnSkipRegister(this, e);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please connect your Camera");
            }
        }

        public static event EventHandler btnSkipRegister;
        private void btnSkip_Click(object sender, EventArgs e)
        {
            if (btnSkipRegister!= null)
            {
                btnSkipRegister(this, e);
            }
        }
    }
}
