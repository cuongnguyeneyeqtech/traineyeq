﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Receptionist
{
    public partial class SignNameRecp : TrainEyeQTech.View.SignName
    {
        public SignNameRecp(SignName signGuest)
        {
            InitializeComponent();
            graphics = pctPaint.CreateGraphics();

            pen = new Pen(Color.Black, 2);
            pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            graphics = pctPaint.CreateGraphics();
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            addControllRecp();
        }

        #region get paint in guest
        public void addControllRecp()
        {
            Guest.SignNameGuest.paintMouseDown += recpMouseDown;
            Guest.SignNameGuest.paintMouseMove += recpMouseMove;
            Guest.SignNameGuest.paintMouseUp += recpMouseUp;
            Guest.SignNameGuest.paintClear += ClearSign;
        }

        public void recpMouseDown(object sender, MouseEventArgs e)
        {
            paint = true;
            x = e.X;
            y = e.Y;
        }
        public void recpMouseMove(object sender, MouseEventArgs e)
        {
            if (paint && x != -1 && y != -1)
            {
                graphics.DrawLine(pen, new Point(x, y), e.Location);
                x = e.X;
                y = e.Y;
            }
        }
        public void recpMouseUp(object sender, MouseEventArgs e)
        {
            paint = false;
            x = -1;
            y = -1;
        }
        private void ClearSign(object sender, EventArgs e)
        {
            graphics.Clear(System.Drawing.SystemColors.ActiveBorder);
            pctPaint.Refresh();
        }
        #endregion get paint in guest

        #region go to other form
        public static event EventHandler btnNextEnd;
        public static event EventHandler btnBackSeclectRoom;
        public override void btnNext_Click(object sender, EventArgs e)
        {
            if (btnNextEnd != null)
                btnNextEnd(sender, e);
        }

        public override void btnBack_Click(object sender, EventArgs e)
        {
            if (btnBackSeclectRoom != null)
                btnBackSeclectRoom(sender, e);
        }
        #endregion go to other form

    }
}
