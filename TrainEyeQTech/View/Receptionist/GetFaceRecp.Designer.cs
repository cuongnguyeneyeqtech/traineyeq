﻿namespace TrainEyeQTech.View.Receptionist
{
    partial class GetFaceRecp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTakePic = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pctCapture)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTakePic
            // 
            this.btnTakePic.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTakePic.Location = new System.Drawing.Point(281, 525);
            this.btnTakePic.Name = "btnTakePic";
            this.btnTakePic.Size = new System.Drawing.Size(124, 30);
            this.btnTakePic.TabIndex = 14;
            this.btnTakePic.Text = "Take Picture";
            this.btnTakePic.UseVisualStyleBackColor = true;
            this.btnTakePic.Click += new System.EventHandler(this.btnTakePic_Click);
            // 
            // GetFaceRecp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.btnTakePic);
            this.Name = "GetFaceRecp";
            this.Controls.SetChildIndex(this.pctCapture, 0);
            this.Controls.SetChildIndex(this.btnTakePic, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pctCapture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTakePic;
    }
}
