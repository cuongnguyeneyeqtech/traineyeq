﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Receptionist
{
    public partial class EndRecp : TrainEyeQTech.View.EndScreen
    {
        public EndRecp()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public static event EventHandler btnBackToStart;
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (btnBackToStart != null)
                btnBackToStart(sender, e);
        }

        public override void EndScreen_Load(object sender, EventArgs e)
        {
            lbWait.Text = "Take the card give to guest";
        }
    }
}
