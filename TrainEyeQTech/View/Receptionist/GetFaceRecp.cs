﻿using AForge.Video;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainEyeQTech.ViewModel;

namespace TrainEyeQTech.View.Receptionist
{
    public partial class GetFaceRecp : TrainEyeQTech.View.CameraScreen
    {
        private CameraScreen guestPage;

        public GetFaceRecp(CameraScreen guestPage)
        {
            InitializeComponent();
            this.guestPage = guestPage;
        }

        public override void Cap(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap src = (Bitmap)eventArgs.Frame.Clone();

            int h = pctCapture.Height;
            Double ratio = Convert.ToDouble(h) / src.Height;
            int w = Convert.ToInt32(ratio * src.Width);

            Bitmap resized_height = new Bitmap(src, new Size(w, h));

            Rectangle cropRect = new Rectangle((resized_height.Width - pctCapture.Width) / 2 - 20, 0, pctCapture.Width, pctCapture.Height);

            src = new Bitmap(cropRect.Width, cropRect.Height);

            using (Graphics g = Graphics.FromImage(src))
            {
                g.DrawImage(resized_height, new Rectangle(0, 0, src.Width, src.Height),
                                 cropRect,
                                 GraphicsUnit.Pixel);
            }

            src.RotateFlip(RotateFlipType.Rotate180FlipY);

            guestPage.pctCapture.Image = src;
            pctCapture.Image = src;
        }

        public static event EventHandler btnQueryClick;

        private void runVM(object obj)
        {
            QueryVM queryGuest = (QueryVM)obj;


            queryGuest.QueryCommand.Execute(pctCapture.Image);


            if (queryGuest.Successed)
            {
                InfGuest = queryGuest.InfGuest;
            }
            else
            {
                MessageBox.Show(queryGuest.Message);
            }

        }
        private void btnTakePic_Click(object sender, EventArgs e)
        {
            if (pctCapture.Image != null)
            {
                Screens.ReceptionistScreen.videoCap.SignalToStop();

                QueryVM queryGuest = new QueryVM(pctCapture.Image);

                Thread vm = new Thread(new ParameterizedThreadStart(runVM));
                vm.Start(queryGuest);

                Thread.Sleep(5000);

                if (btnQueryClick != null && queryGuest.Successed)
                    btnQueryClick(sender, e);
                else
                    Screens.ReceptionistScreen.videoCap.Start();
            }
            else
            {
                MessageBox.Show("Please connect your Camera");
            }
        }
    }
}
