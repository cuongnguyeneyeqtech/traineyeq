﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Receptionist
{
    public partial class QueryInfRecp : Form
    {

        public QueryInfRecp(DataTable InfGuest)
        {
            InitializeComponent();

            dataInfGuest.DataBindings.Clear();
            dataInfGuest.DataSource = InfGuest;
            dataInfGuest.AllowUserToAddRows = false;
            dataInfGuest.RowHeadersVisible = false;
        }
        public static event EventHandler btnBackClick;
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (btnBackClick != null)
            {
                btnBackClick(sender, e);
            }
            
        }
        public static event DataGridViewCellEventHandler chooseGuest;
        private void dataInfGuest_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (chooseGuest != null)
                chooseGuest(sender, e);
        }

    }
}
