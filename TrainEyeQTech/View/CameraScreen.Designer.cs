﻿namespace TrainEyeQTech.View
{
    partial class CameraScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pctCapture = new TrainEyeQTech.View.OvalPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctCapture)).BeginInit();
            this.SuspendLayout();
            // 
            // pctCapture
            // 
            this.pctCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pctCapture.Location = new System.Drawing.Point(167, 155);
            this.pctCapture.Name = "pctCapture";
            this.pctCapture.Size = new System.Drawing.Size(350, 350);
            this.pctCapture.TabIndex = 2;
            this.pctCapture.TabStop = false;
            // 
            // CameraScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.pctCapture);
            this.Name = "CameraScreen";
            this.Text = "CameraScreen";
            ((System.ComponentModel.ISupportInitialize)(this.pctCapture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public OvalPictureBox pctCapture;
    }
}