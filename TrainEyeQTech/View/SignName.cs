﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainEyeQTech.View
{
    public partial class SignName : Form
    {
        protected Graphics graphics;
        protected int x = -1;
        protected int y = -1;
        protected bool paint;
        protected Pen pen;
        public SignName()
        {
            InitializeComponent();
        }

        #region paint

        public virtual void pctPaint_MouseDown(object sender, MouseEventArgs e)
        {}

        public virtual void pctPaint_MouseMove(object sender, MouseEventArgs e)
        {}
        public virtual void pctPaint_MouseUp(object sender, MouseEventArgs e)
        {}
        #endregion paint


        #region controller
        public virtual void btnNext_Click(object sender, EventArgs e)
        {}

        public virtual void btnBack_Click(object sender, EventArgs e)
        {}
        #endregion controller

        public virtual void SignName_Load(object sender, EventArgs e)
        {}
    }
}
