﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Guest
{
    public partial class SignNameGuest : TrainEyeQTech.View.SignName
    {
        public SignNameGuest()
        {
            InitializeComponent();
            pen = new Pen(Color.Black, 2);
            pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            graphics = pctPaint.CreateGraphics();
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        }

        #region paint
        public static event MouseEventHandler paintMouseDown;
        public static event MouseEventHandler paintMouseMove;
        public static event MouseEventHandler paintMouseUp;
        public static event EventHandler paintClear;
        public override void pctPaint_MouseDown(object sender, MouseEventArgs e)
        {
            if (paintMouseDown != null)
                paintMouseDown(sender, e);
            paint = true;
            x = e.X;
            y = e.Y;
        }

        public override void pctPaint_MouseUp(object sender, MouseEventArgs e)
        {
            if (paintMouseUp != null)
                paintMouseUp(sender, e);
            paint = false;
            x = -1;
            y = -1;
        }

        public override void pctPaint_MouseMove(object sender, MouseEventArgs e)
        {
            if (paintMouseMove != null)
                paintMouseMove(sender, e);
            if (paint && x != -1 && y != -1)
            {
                graphics.DrawLine(pen, new Point(x, y), e.Location);
                x = e.X;
                y = e.Y;
            }
        }

        
        public override void btnBack_Click(object sender, EventArgs e)
        {
            if (paintClear != null)
                paintClear(sender, e);
            graphics.Clear(System.Drawing.SystemColors.ActiveBorder);
            pctPaint.Refresh();
        }

        #endregion paint

        public override void SignName_Load(object sender, EventArgs e)
        {
            btnNext.Visible = false;
            btnBack.Text = "Clear";
            btnBack.Location = new Point((btnBack.Location.X + btnNext.Location.X - btnBack.Size.Width/2)/2, btnBack.Location.Y);
            btnBack.Name = "btnClear";
        }
    }
}
