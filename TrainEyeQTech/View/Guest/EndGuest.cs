﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Guest
{
    public partial class EndGuest : TrainEyeQTech.View.EndScreen
    {
        public EndGuest()
        {
            InitializeComponent();
        }

        public override void EndScreen_Load(object sender, EventArgs e)
        {
            lbWait.Text = "Please take the key card from our receptionist\nHave a great holiday!";
        }
    }
}
