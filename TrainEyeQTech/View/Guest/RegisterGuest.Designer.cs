﻿namespace TrainEyeQTech.View.Guest
{
    partial class RegisterGuest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbWait = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbWait
            // 
            this.lbWait.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbWait.AutoSize = true;
            this.lbWait.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWait.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbWait.Location = new System.Drawing.Point(162, 71);
            this.lbWait.Name = "lbWait";
            this.lbWait.Size = new System.Drawing.Size(360, 31);
            this.lbWait.TabIndex = 16;
            this.lbWait.Text = "Please put your face in circle";
            // 
            // RegisterGuest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.lbWait);
            this.Name = "RegisterGuest";
            this.Controls.SetChildIndex(this.lbWait, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbWait;
    }
}
