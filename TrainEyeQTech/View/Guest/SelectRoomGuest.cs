﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Guest
{
    public partial class SelectRoomGuest : TrainEyeQTech.View.SelectRoom
    {
        public SelectRoomGuest()
        {
            InitializeComponent();
        }

        public override void SelectRoom_Load(object sender, EventArgs e)
        {
            lbTitle.Text = "Please Select Your Room By Touch It";
        }
    }
}
