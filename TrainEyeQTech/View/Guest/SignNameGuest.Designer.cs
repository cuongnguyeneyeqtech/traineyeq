﻿namespace TrainEyeQTech.View.Guest
{
    partial class SignNameGuest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.pctPaint)).BeginInit();
            this.SuspendLayout();
            // 
            // pctPaint
            // 
            this.pctPaint.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pctPaint_MouseDown);
            this.pctPaint.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pctPaint_MouseMove);
            this.pctPaint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pctPaint_MouseUp);
            // 
            // btnBack
            // 
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // SignNameGuest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Name = "SignNameGuest";
            this.Load += new System.EventHandler(this.SignName_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctPaint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
