﻿namespace TrainEyeQTech.View
{
    partial class SignName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pctPaint = new System.Windows.Forms.PictureBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pctPaint)).BeginInit();
            this.SuspendLayout();
            // 
            // pctPaint
            // 
            this.pctPaint.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pctPaint.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pctPaint.Location = new System.Drawing.Point(150, 150);
            this.pctPaint.Name = "pctPaint";
            this.pctPaint.Size = new System.Drawing.Size(400, 400);
            this.pctPaint.TabIndex = 0;
            this.pctPaint.TabStop = false;
            this.pctPaint.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pctPaint_MouseDown);
            this.pctPaint.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pctPaint_MouseMove);
            this.pctPaint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pctPaint_MouseUp);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBack.Location = new System.Drawing.Point(218, 586);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(120, 50);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnNext.Location = new System.Drawing.Point(363, 586);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(120, 50);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // SignName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.pctPaint);
            this.Name = "SignName";
            this.Text = "SignName";
            this.Load += new System.EventHandler(this.SignName_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctPaint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox pctPaint;
        public System.Windows.Forms.Button btnBack;
        public System.Windows.Forms.Button btnNext;
    }
}