﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Toolkit.Mvvm;

namespace TrainEyeQTech.View.Screens
{
    public partial class GuestScreen : TrainEyeQTech.View.Screens.frmShow
    {
        //#region
        //public CameraScreen registerGuest;
        //#endregion

        public GuestScreen()
        {
            InitializeComponent();
            registerGuest = new Guest.RegisterGuest();

            signNameGuest = new Guest.SignNameGuest();
        }

        #region controll child form


        //controll get face form
        public override void ToGuestInf(object sender, EventArgs e)
        {
            queryInfGuest = new Guest.QueryInfGuest();
            showChildForm(queryInfGuest, pnlShow);
        }

        public override void ToGetFace(object sender, EventArgs e)
        {
            showChildForm(getfacerGuest, pnlShow);
        }
        public override void ToChooseRoom(object sender, EventArgs e)
        {
            selectRoomGuest = new Guest.SelectRoomGuest();
            showChildForm(selectRoomGuest, pnlShow);
        }
        public override void ToSignPage(object sender, EventArgs e)
        {
            showChildForm(signNameGuest, pnlShow);
        }

        public override void ToTheEnd(object sender, EventArgs e)
        {
            endGuest = new Guest.EndGuest();
            showChildForm(endGuest, pnlShow);
        }
        //to start form
        public override void BackToStart(object sender, EventArgs e)
        {
            showChildForm(registerGuest, pnlShow);
        }
        #endregion controll child form

        #region select room
        public override void SelectedRoom(Button btn)
        {
            foreach (Control ctrl in selectRoomGuest.pnlRoom.Controls)
            {
                if (ctrl.GetType() == typeof(Button))
                {
                    if (ctrl.Name != btn.Name)
                        ctrl.BackColor = System.Drawing.SystemColors.Control;
                    else
                        ctrl.BackColor = System.Drawing.SystemColors.ActiveCaption;
                }
            }
        }
        //choose room
        public override void ClickRoom(object sender, EventArgs e)
        {
            SelectedRoom((Button)sender);
        }
        #endregion select room

        #region load form
        public override void frmShow_Load(object sender, EventArgs e)
        {
            showChildForm(registerGuest, pnlShow);
        }
        #endregion
    }
}
