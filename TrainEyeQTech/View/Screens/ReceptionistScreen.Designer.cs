﻿using AForge.Video.DirectShow;
using System.Windows.Forms;

namespace TrainEyeQTech.View.Screens
{
    partial class ReceptionistScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.cameraMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCamera = new System.Windows.Forms.MenuStrip();
            this.pnlShow.SuspendLayout();
            this.menuCamera.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlShow
            // 
            this.pnlShow.Controls.Add(this.menuCamera);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // cameraMenu
            // 
            this.cameraMenu.Name = "cameraMenu";
            this.cameraMenu.Size = new System.Drawing.Size(60, 20);
            this.cameraMenu.Text = "Camera";
            // 
            // menuCamera
            // 
            this.menuCamera.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cameraMenu});
            this.menuCamera.Location = new System.Drawing.Point(0, 0);
            this.menuCamera.Name = "menuCamera";
            this.menuCamera.Size = new System.Drawing.Size(684, 24);
            this.menuCamera.TabIndex = 0;
            this.menuCamera.Text = "menuStrip1";
            // 
            // ReceptionistScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.MainMenuStrip = this.menuCamera;
            this.Name = "ReceptionistScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmShow_FormClosing);
            this.Load += new System.EventHandler(this.frmShow_Load);
            this.pnlShow.ResumeLayout(false);
            this.pnlShow.PerformLayout();
            this.menuCamera.ResumeLayout(false);
            this.menuCamera.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private MenuStrip menuCamera;
        private ToolStripMenuItem cameraMenu;
    }
}
