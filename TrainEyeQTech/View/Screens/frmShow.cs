﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Toolkit.Mvvm;
using TrainEyeQTech.View.Guest;
using TrainEyeQTech.View.Receptionist;

namespace TrainEyeQTech.View.Screens
{
    public partial class frmShow : Form
    {
        #region
        public CameraScreen registerRecp;
        public CameraScreen registerGuest;
        public CameraScreen getfaceRecp;
        public CameraScreen getfacerGuest;
        public QueryInfGuest queryInfGuest;
        public QueryInfRecp queryInfRecp;
        public SelectRoom selectRoomRecp;
        public SelectRoom selectRoomGuest;
        public SignName signNameRecp;
        public SignName signNameGuest;
        public EndScreen endRecp;
        public EndScreen endGuest;
        #endregion

        protected void showChildForm(Form frmChild, Panel pnl)
        {
            frmChild.TopLevel = false;
            frmChild.FormBorderStyle = FormBorderStyle.None;
            frmChild.Dock = DockStyle.Fill;
            pnl.Controls.Add(frmChild);
            pnl.Tag = frmChild;
            frmChild.BringToFront();
            frmChild.Show();
        }

        public frmShow()
        {
            InitializeComponent();
            #region controll form
            Receptionist.RegisterRecp.btnSkipRegister += ToGetFace;
            Receptionist.QueryInfRecp.btnBackClick += ToGetFace;
            Receptionist.GetFaceRecp.btnQueryClick += ToGuestInf;
            Receptionist.QueryInfRecp.chooseGuest += ToChooseRoom;
            Receptionist.SelectRoomRecp.btnNextSign += ToSignPage;
            Receptionist.SelectRoomRecp.btnPrevInfGuest += ToGuestInf;
            Receptionist.SignNameRecp.btnNextEnd += ToTheEnd;
            Receptionist.SignNameRecp.btnBackSeclectRoom += ToChooseRoom;
            Receptionist.EndRecp.btnBackToStart += BackToStart;
            #endregion controll form

            //click room
            SelectRoom.btnRoomClick += ClickRoom;
        }

        #region load closing screen
        public virtual void frmShow_Load(object sender, EventArgs e)
        { }

        public virtual void frmShow_FormClosing(object sender, FormClosingEventArgs e)
        { }
        #endregion
        #region controll child page
        public virtual void ToGetFace(object sender, EventArgs e) { }
        public virtual void ToGuestInf(object sender, EventArgs e) { }
        public virtual void ToChooseRoom(object sender, EventArgs e) { }
        public virtual void ToSignPage(object sender, EventArgs e) { }
        public virtual void ToTheEnd(object sender, EventArgs e) { }
        public virtual void BackToStart(object sender, EventArgs e) { }
        #endregion

        //choose room
        public virtual void ClickRoom(object sender, EventArgs e) { }

        public virtual void SelectedRoom(Button btn) { }


    }
}
