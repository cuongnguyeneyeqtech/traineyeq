﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Toolkit.Mvvm;
using TrainEyeQTech.ViewModel;

namespace TrainEyeQTech.View.Screens
{
    public partial class ReceptionistScreen : TrainEyeQTech.View.Screens.frmShow
    {
        public static VideoCaptureDevice videoCap;
        private FilterInfoCollection filterInfo;
        private frmShow frmGuest;

        public ReceptionistScreen()
        {
            InitializeComponent();

            timer.Start();
            frmGuest = new GuestScreen();
            //show secondary screen
            registerRecp = new Receptionist.RegisterRecp(frmGuest.registerGuest);

            //set up camera
            filterInfo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (filterInfo.Count > 0)
                videoCap = new VideoCaptureDevice(filterInfo[0].MonikerString);
            else
            {
                MessageBox.Show("Please buy the Camera :v");
                Application.Exit();
            }
        }

        #region load - close form
        int pre_camera = -1;
        private void timer_Tick(object sender, EventArgs e)
        {
            filterInfo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            int num = filterInfo.Count;
            if (num != pre_camera)
            {
                LoadCamera();
                if (num < pre_camera)
                {
                    TakeCamera(filterInfo[0]);
                }
            }
            pre_camera = num;
            showTwoScreen(frmGuest);
        }
        public override void frmShow_Load(object sender, EventArgs e)
        {
            //LoadCamera();
            resetCamera(registerRecp);

            //show 2 screens
            //showTwoScreen(frmGuest);
            frmGuest.Show();

            showChildForm(registerRecp, pnlShow);
        }
        public override void frmShow_FormClosing(object sender, FormClosingEventArgs e)
        {
            videoCap.Stop();
            timer.Stop();
            Application.Exit();
        }
        #endregion

        #region setup camera
        private void LoadCamera()
        {
            cameraMenu.DropDownItems.Clear();
            filterInfo = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            ToolStripMenuItem[] items = new ToolStripMenuItem[filterInfo.Count]; // You would obviously calculate this value at runtime
            for (int i = 0; i < items.Length; i++)
            {
                items[i] = new ToolStripMenuItem();
                items[i].Name = "Camera" + i.ToString();
                items[i].Tag = filterInfo[i];
                items[i].Text = "Camera" + (i+1).ToString();
                items[i].Click += new EventHandler(MenuItemClickHandler);
            }
            cameraMenu.DropDownItems.AddRange(items);
        }
        private void MenuItemClickHandler(object sender, EventArgs e)
        {
            ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;

            TakeCamera((FilterInfo)clickedItem.Tag);
        }
        private void TakeCamera(FilterInfo filterInf)
        {
            if (videoCap.IsRunning)
                videoCap.Stop();

            videoCap = new VideoCaptureDevice(((FilterInfo)filterInf).MonikerString);

            if (getfaceRecp != null)
                videoCap.NewFrame += getfaceRecp.Cap;
            if (registerRecp != null)
                videoCap.NewFrame += registerRecp.Cap;

            videoCap.Start();
        }
        #endregion


        #region controll camera        
        public void resetCamera(CameraScreen recpPage)
        {
            if (videoCap.IsRunning)
                videoCap.Stop();

            videoCap.NewFrame += recpPage.Cap;
            videoCap.Start();
        }

        #endregion

        #region two screen
        //set 2 screen
        private void showTwoScreen(frmShow form)
        {
            Screen[] screens = Screen.AllScreens;
            // if just only one screen show all in screen one
            try
            {
                setFormLocation(form, screens[1]);
            }
            catch
            {
                setFormLocation(form, screens[0]);
            }
        }
        private void setFormLocation(Form form, Screen screen)
        {
            Rectangle bounds = screen.Bounds;
            form.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            //form.Show();
        }
        #endregion

        #region controll form
        //func
        public void GoToGetFace()
        {
            menuCamera.Visible = true;
            frmGuest.getfacerGuest = new Guest.GetFaceGuest();
            getfaceRecp = new Receptionist.GetFaceRecp(frmGuest.getfacerGuest);
            resetCamera(getfaceRecp);
            showChildForm(getfaceRecp, pnlShow);
        }
        public void ToQuery()
        {
            videoCap.Stop();
            queryInfRecp = new Receptionist.QueryInfRecp(getfaceRecp.InfGuest);
        }
        //controll get face form
        public override void ToGuestInf(object sender, EventArgs e)
        {
            menuCamera.Visible = false;
            ToQuery();
            showChildForm(queryInfRecp, pnlShow);
        }

        //controll result of query and controll register form
        public override void ToGetFace(object sendrer, EventArgs e)
        {
            GoToGetFace();
        }        
        public override void ToChooseRoom(object sender, EventArgs e)
        {
            selectRoomRecp = new Receptionist.SelectRoomRecp();
            showChildForm(selectRoomRecp, pnlShow);
        }
        // controll choose room
        public override void ToSignPage(object sender, EventArgs e)
        {
            signNameRecp = new Receptionist.SignNameRecp(frmGuest.signNameGuest);
            showChildForm(signNameRecp, pnlShow);
        }

        //controll end form
        public override void ToTheEnd(object sender, EventArgs e)
        {
            endRecp = new Receptionist.EndRecp();
            showChildForm(endRecp, pnlShow);
        }
        public override void BackToStart(object sender, EventArgs e)
        {
            menuCamera.Visible = true;
            resetCamera(registerRecp);
            showTwoScreen(frmGuest);
            showChildForm(registerRecp, pnlShow);
        }
        #endregion controll child form

        #region select room
        public override void SelectedRoom(Button btn)
        {
            foreach (Control ctrl in selectRoomRecp.pnlRoom.Controls)
            {
                if (ctrl.GetType() == typeof(Button))
                {
                    if (ctrl.Name != btn.Name)
                        ctrl.BackColor = System.Drawing.SystemColors.Control;
                    else
                        ctrl.BackColor = System.Drawing.SystemColors.ActiveCaption;
                }
            }
        }
        //choose room
        public override void ClickRoom(object sender, EventArgs e)
        {
            SelectedRoom((Button)sender);
        }

        #endregion select room        
 
    }
}
